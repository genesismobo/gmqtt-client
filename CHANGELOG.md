# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.2.0] - 2021-11-30

Better documentation and cleanups.

### Added
- Better README.md
- Function *utf8(bool)* in *MessageBuilder*

### Changed
- **[BREAKING]** Hidden unnecessary exports and structs from *mqttbytes*
- **[BREAKING]** Removed prefix *set_* from functions in *MqttClientBuilder*
- **[BREAKING]** Removed function *set_mqtt_broker* from *MqttClientBuilder* and replaced with *new(Url)*
- **[BREAKING]** Bumped `mqttbytes` to 0.6
- *publish_json(..)* sets Message Payload Indicator to 1
 
### Fixed

## [0.1.0] - 2021-11-08

Initial release

### Added

### Changed

### Fixed
