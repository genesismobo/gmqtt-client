use std::time::Duration;

use anyhow::Result;
use gmqtt_client::{MqttClientBuilder, QoS};
use log::{info, LevelFilter};
use tokio::runtime;

const MQTT_BROKER: &str = "tcp://localhost:1883";

fn main() -> Result<()> {
    env_logger::Builder::new()
        .filter_level(LevelFilter::Debug)
        .init();

    let (mqtt_client, mqtt_worker) = MqttClientBuilder::new(MQTT_BROKER.parse()?)
        .on_connected_callback(on_mqtt_connected)
        .client_id("example-client")
        .build();

    let worker_runtime = runtime::Builder::new_multi_thread()
        .enable_all()
        .worker_threads(1)
        .build()?;

    worker_runtime.spawn(async { mqtt_worker.run().await });

    let mut counter = 0;

    loop {
        let message = serde_json::json!({ "counter": counter });

        mqtt_client.publish_json(
            "client/example/pubsub",
            &message,
            false,
            QoS::AtLeastOnce,
            Some(Duration::from_secs(1)),
        )?;

        info!("Example signals published");

        counter += 1;

        std::thread::sleep(Duration::from_secs(1));
    }
}

fn on_mqtt_connected() {
    info!("MQTT connected");
}
